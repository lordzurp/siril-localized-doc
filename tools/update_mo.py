import os
from pathdefs import docs, base
from pathlib import Path
import shutil
from subprocess import Popen, PIPE, STDOUT



def main():

    # collect templates names from locale/
    print('gathering fake mos')
    os.chdir(os.path.join(base, 'po'))
    templates = Path('.').glob('*.pot')
    fake_mos=[str(t).replace('.pot','.mo') for t in templates]

    # collect languages
    os.chdir(docs)
    dirs = Path('.').glob('*')
    dirs_list = [str(d) for d in dirs]

    # updating all mo files in po/ and symlinked fake mo files in translated/lang/LC_MESSAGES
    for d in dirs_list:
        os.chdir(os.path.join(base, 'po'))
        pofile = '{:}.po'.format(d)
        mofile = '{:}.mo'.format(d)
        print('Updating {:s}'.format(mofile))

        cmd = 'msgfmt {:s} -o {:s}'.format(pofile, mofile)
        p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
        p.communicate()
        os.chdir(os.path.join(base, 'translated', d))
        if Path('.').joinpath('LC_MESSAGES').is_dir():
            shutil.rmtree('LC_MESSAGES')
        try:
            Path('.').joinpath('LC_MESSAGES').mkdir()
        except Exception as err:
            print(err)
        os.chdir('./LC_MESSAGES')
        src = os.path.join(base, 'po', mofile)

        for f in fake_mos:
            dst = os.path.join(os.path.join(base, 'translated', d, 'LC_MESSAGES', f))
            srcrel = os.path.relpath(src, os.path.split(dst)[0])
            os.symlink(srcrel, dst)



if __name__ == "__main__":
    main()
