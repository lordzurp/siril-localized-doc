import os,sys
from pathdefs import base
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT

def main():

    os.chdir(os.path.join(base, 'po'))
    pos = Path('.').glob('*.po')

    # querying po files
    for p in pos:
        pofile = p.name
        print('Statistics from {:s}'.format(pofile))
        if sys.platform.startswith('win32'):
            output = 'nul'
        else:
            output = '/dev/null'
        cmd = 'msgfmt --statistics {:s} -o {:s}'.format(pofile, output)
        p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
        print('{:s}'.format(p.communicate()[0].decode()))


if __name__ == "__main__":
    main()
